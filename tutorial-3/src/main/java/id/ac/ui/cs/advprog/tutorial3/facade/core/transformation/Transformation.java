package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

// An interface for Transformation, used in Translator Class to make
// a non-harcoded implementation of encode and decode for each transformation
public interface Transformation {
    public Spell encode(Spell spell);
    public Spell decode(Spell spell);
}

package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WeaponServiceImpl implements WeaponService {

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    private Boolean isFirst = true; // Emulate a constructor


    @Override
    public List<Weapon> findAll() {
        // Adapt bow and spellbook to weapon and then load to weapon repo.
        // Only do this one time
        if (this.isFirst) {
            for (Bow bow : bowRepository.findAll()) {
                weaponRepository.save(new BowAdapter(bow));
            }
            for (Spellbook spellbook : spellbookRepository.findAll()) {
                weaponRepository.save(new SpellbookAdapter(spellbook));
            }

            this.isFirst = false;
        }

        // Return all weapon on weapon repo with findAll method
        // which on this point already contain all weapon, spellbook, and bow
        return weaponRepository.findAll();
    }

    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        // Find weapon on weapon repo
        Weapon weapon = weaponRepository.findByAlias(weaponName);

        // Prepare log according to README
        String log = "";
        if (attackType == 1) {
            log = weapon.getHolderName() + " attack with " +
                    weapon.getName() + " (charged attack): " +
                    weapon.chargedAttack();
        } else if (attackType == 0) {
            log = weapon.getHolderName() + " attack with " +
                    weapon.getName() + " (normal attack): " +
                    weapon.normalAttack();
        }

        // Save modified log and weapon
        logRepository.addLog(log);
        weaponRepository.save(weapon);
    }

    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}

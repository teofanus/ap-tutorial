package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private Boolean consecutiveChargedAttackDetector;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.consecutiveChargedAttackDetector = false;
    }

    // Normal attack calls smallSpell on spellbook
    @Override
    public String normalAttack() {
        this.consecutiveChargedAttackDetector = false;
        return spellbook.smallSpell();
    }

    // You can't fire consecutive charged attack (which calls largeSpell method
    // on spellbook
    @Override
    public String chargedAttack() {
        if (consecutiveChargedAttackDetector) {
            this.consecutiveChargedAttackDetector = false;
            return "Have you read the manual? You can't fire consecutive charged attack dummy.";
        } else {
            this.consecutiveChargedAttackDetector = true;
            return spellbook.largeSpell();
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}

package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AbyssalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CelestialTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.InfernalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation;

import java.util.ArrayList;
import java.util.List;

public class Translator {
    private List<Transformation> transformationList;

    // Don't hardcode decode and encode, instead add all transformation to a list
    // of transformations
    public Translator(){
        this.transformationList = new ArrayList<>();

        this.transformationList.add(new AbyssalTransformation());
        this.transformationList.add(new CelestialTransformation());
        this.transformationList.add(new InfernalTransformation());
    }

    public String encode(String text) {
        // Encoding flow according to the guide in FacadeServiceImpl class
        Spell spell = new Spell(text, AlphaCodex.getInstance());

        // Use the list to call encode on each transformations
        for (Transformation transformation : transformationList) {
            transformation.encode(spell);
        }

        spell = CodexTranslator.translate(spell, RunicCodex.getInstance());

        return spell.getText();
    }

    public String decode(String code) {
        // Decoding flow according to the guide in FacadeServiceImpl class
        Spell spell = new Spell(code, RunicCodex.getInstance());

        spell = CodexTranslator.translate(spell, AlphaCodex.getInstance());

        // Use the list to call decode on each transformations
        for (int i = transformationList.size(); i > 0; i--) {
            transformationList.get(i - 1).decode(spell);
        }

        return spell.getText();
    }
}

package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

public class StaffOfHoumo implements Weapon {

    private String holderName;

    public StaffOfHoumo(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public String normalAttack() {
        return "Staff of Houmooooo";
    }

    @Override
    public String chargedAttack() {
        return "Here comes a charged staff of houmo attack";
    }

    @Override
    public String getName() {
        return "Here comes a charged staff of houmo attack";
    }

    @Override
    public String getHolderName() { return holderName; }
}

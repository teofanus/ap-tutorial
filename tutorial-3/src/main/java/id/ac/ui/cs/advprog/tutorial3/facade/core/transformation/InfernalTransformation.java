package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

/**
 * Kelas ini mengimplementasikan sistem kriptografi Caesar Cipher
 */
public class InfernalTransformation implements Transformation{
    private int key;

    public InfernalTransformation(int transition) {
        this.key = transition;
    }

    public InfernalTransformation() {
        this(13);
    }

    public Spell encode(Spell spell) {
        return process(spell, true);
    }

    public Spell decode(Spell spell) {
        return process(spell, false);
    }

    public Spell process(Spell spell, boolean encode) {
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int selector = encode ? this.key : (26 - this.key);
        selector = selector % 26 + 26;
        StringBuilder res = new StringBuilder();

        for(Character each : text.toCharArray()){
            if (Character.isLetter(each)) {
                if (Character.isUpperCase(each)) {
                    res.append((char) ((each + selector - 'A') % 26 + 'A'));
                } else {
                    res.append((char) ((each + selector - 'a') % 26 + 'a'));
                }
            } else {
                res.append(each);
            }
        }

        return new Spell(res.toString(), codex);
    }
}

package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class InfernalTransformationTest {
    private Class<?> infernalClass;

    @BeforeEach
    public void setup() throws Exception {
        infernalClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.InfernalTransformation");
    }

    @Test
    public void testInfernalHasEncodeMethod() throws Exception {
        Method translate = infernalClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testInfernalEncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Fnsven naq V jrag gb n oynpxfzvgu gb sbetr bhe fjbeq";

        Spell result = new InfernalTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testInfernalEncodesCorrectlyWithCustomKey() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Tbgjsb boe J xfou up b cmbdltnjui up gpshf pvs txpse";

        Spell result = new InfernalTransformation(1).encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testInfernalHasDecodeMethod() throws Exception {
        Method translate = infernalClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testInfernalDecodesCorrectly() throws Exception {
        String text = "Fnsven naq V jrag gb n oynpxfzvgu gb sbetr bhe fjbeq";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new InfernalTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testInfernalDecodesCorrectlyWithCustomKey() throws Exception {
        String text = "Tbgjsb boe J xfou up b cmbdltnjui up gpshf pvs txpse";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new InfernalTransformation(1).decode(spell);
        assertEquals(expected, result.getText());
    }
}

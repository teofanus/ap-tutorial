package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {

	@Override
	public String getType() {
		return "Defend With Barrier";
	}

	@Override
	public String defend() {
		return "Invisible barrier, go forth!";
	}
    
}

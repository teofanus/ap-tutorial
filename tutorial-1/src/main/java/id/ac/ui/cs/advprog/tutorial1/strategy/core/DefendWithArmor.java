package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {

    @Override
    public String getType() {
        return "Defend With Armor";
    }

    @Override
    public String defend() {
        return "Hey, this armor is not that bad after all!";
    }
    
}

package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
    }

    @Override
    public void update() {
        // Add quest if delivery or rumble type
        if (guild.getQuestType().equals("D") || guild.getQuestType().equals("R")) {
            getQuests().add(guild.getQuest());
        }
    }
}

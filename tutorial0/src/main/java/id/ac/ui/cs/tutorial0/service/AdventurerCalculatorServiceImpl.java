package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class AdventurerCalculatorServiceImpl implements AdventurerCalculatorService {
    
    /**
     * Calculate power from given adventurer birthyear
     */
    @Override
    public int countPowerPotensialFromBirthYear(int birthYear) {
        int rawAge = getRawAge(birthYear);
        if (rawAge < 30) {
            return rawAge * 2000;
        } else if (rawAge < 50) {
            return rawAge * 2250;
        } else {
            return rawAge * 5000;
        }
    }

    /**
     * Calcualte power class according to adventurer power 
     */
    @Override
    public char getPowerClassFromPower(int power) {
        if (power >= 0 && power <= 20000) {
            return 'C';
        } else if (power > 20000 && power <= 100000) {
            return 'B';
        } else {
            return 'A';
        }
    }

    /**
     * Calculate adventurer age from birth year
     */
    private int getRawAge(int birthYear) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear - birthYear;
    }
}

package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuTest {
    private Menu menu = new InuzumaRamen("InuzumaRamen");

    @Test
    public void testGetName() {
        assertEquals("InuzumaRamen", menu.getName());
    }

    @Test
    public void testGetNoodle() {
        assertTrue(menu.getNoodle() instanceof Ramen);
    }

    @Test
    public void testGetMeat() {
        assertTrue(menu.getMeat() instanceof Pork);
    }

    @Test
    public void testGetTopping() {
        assertTrue(menu.getTopping() instanceof BoiledEgg);
    }

    @Test
    public void testGetFlavor() {
        assertTrue(menu.getFlavor() instanceof Spicy);
    }

}

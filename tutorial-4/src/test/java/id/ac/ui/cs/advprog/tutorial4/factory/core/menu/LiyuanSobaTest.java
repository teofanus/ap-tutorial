package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LiyuanSobaTest {
    private LiyuanSoba menu;

    @BeforeEach
    public void setup() {
        menu = new LiyuanSoba("Liyuan Soba");
    }

    @Test
    public void testGetName() {
        assertEquals("Liyuan Soba", menu.getName());
    }

    @Test
    public void testGetNoodle() {
        assertEquals(Soba.class, menu.getNoodle().getClass());
    }

    @Test
    public void testGetMeat() {
        assertEquals(Beef.class, menu.getMeat().getClass());
    }

    @Test
    public void testGetTopping() {
        assertEquals(Mushroom.class, menu.getTopping().getClass());
    }

    @Test
    public void testGetFlavor() {
        assertEquals(Sweet.class, menu.getFlavor().getClass());
    }
}

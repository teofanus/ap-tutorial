package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MenuServiceTest {
    @Spy
    private MenuRepository repo;

    @InjectMocks
    private MenuServiceImpl menuService;

    @BeforeEach
    public void setup() {
        repo.getMenus().clear();
    }

    @Test
    public void testInitClass() {
        menuService = new MenuServiceImpl();
        verify(repo, times(4)).add(any(Menu.class));
    }

    @Test
    public void testGetMenus() {
        List<Menu> mockMenu = new ArrayList<>();
        mockMenu.add(new LiyuanSoba("LiyuanSoba"));
        mockMenu.add(new MondoUdon("MondoUdon"));

        when(repo.getMenus()).thenReturn(mockMenu);

        List<Menu> menus = menuService.getMenus();
        verify(repo, atLeast(1)).getMenus();
        assertEquals(mockMenu, menus);
    }

    @Test
    public void testCreateMenu() {
        menuService.createMenu("LiyuanSoba", "LiyuanSoba");
        verify(repo, atLeast(1)).add(any(LiyuanSoba.class));
        assertEquals(1, repo.getMenus().size());
    }
}


package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.factory.InuzumaRamenIngredientsFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class InuzumaRamenIngredientsFactoryTest {
    InuzumaRamenIngredientsFactory factory;

    @BeforeEach
    public void setup() {
        factory = new InuzumaRamenIngredientsFactory();
    }

    @Test
    public void testCreateNoodleShouldReturnRamen() {
        assertEquals(Ramen.class, factory.createNoodle().getClass());
    }

    @Test
    public void testCreateMeatShouldReturnPork() {
        assertEquals(Pork.class, factory.createMeat().getClass());
    }

    @Test
    public void testCreateToppingShouldReturnBoiledEgg() {
        assertEquals(BoiledEgg.class, factory.createTopping().getClass());
    }

    @Test
    public void testCreateFlavorShouldReturnSpicy() {
        assertEquals(Spicy.class, factory.createFlavor().getClass());
    }
}

package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderFoodTest {
    @Test
    public void testNoDuplicateOrderFoodInstance() {
        OrderFood instance1 = OrderFood.getInstance();
        OrderFood instance2 = OrderFood.getInstance();
        assertEquals(instance1, instance2);
    }

    @Test
    public void testDrinkGetterSetter() {
        OrderFood.getInstance().setFood("testDrink");
        assertEquals("testDrink", OrderFood.getInstance().getFood());
    }

    @Test
    public void testToStringMethodReturnDrink() {
        OrderFood.getInstance().setFood("testDrink");
        assertEquals("testDrink", OrderFood.getInstance().toString());
    }

    @Test
    public void testToStringMethodWhenNoFoodIsSet() {
        assertEquals("No Order yet. Order first.", OrderFood.getInstance().toString());
    }
}

package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {
    private OrderServiceImpl orderService = new OrderServiceImpl();

    @Test
    public void testOrderingAFood() {
        orderService.orderAFood("testFood");
        assertEquals("testFood", orderService.getFood().getFood());
    }

    @Test
    public void testOrderingADrink() {
        orderService.orderADrink("testDrink");
        assertEquals("testDrink", orderService.getDrink().getDrink());
    }
}

package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.factory.LiyuanSobaIngredientsFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.factory.MondoUdonIngredientsFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LiyuanSobaIngredientsFactoryTest {
    LiyuanSobaIngredientsFactory factory;

    @BeforeEach
    public void setup() {
        factory = new LiyuanSobaIngredientsFactory();
    }

    @Test
    public void testCreateNoodleShouldReturnRamen() {
        assertEquals(Soba.class, factory.createNoodle().getClass());
    }

    @Test
    public void testCreateMeatShouldReturnPork() {
        assertEquals(Beef.class, factory.createMeat().getClass());
    }

    @Test
    public void testCreateToppingShouldReturnBoiledEgg() {
        assertEquals(Mushroom.class, factory.createTopping().getClass());
    }

    @Test
    public void testCreateFlavorShouldReturnSpicy() {
        assertEquals(Sweet.class, factory.createFlavor().getClass());
    }
}

package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.factory.InuzumaRamenIngredientsFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.factory.MondoUdonIngredientsFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MondoUdonIngredientsFactoryTest {
    MondoUdonIngredientsFactory factory;

    @BeforeEach
    public void setup() {
        factory = new MondoUdonIngredientsFactory();
    }

    @Test
    public void testCreateNoodleShouldReturnRamen() {
        assertEquals(Udon.class, factory.createNoodle().getClass());
    }

    @Test
    public void testCreateMeatShouldReturnPork() {
        assertEquals(Chicken.class, factory.createMeat().getClass());
    }

    @Test
    public void testCreateToppingShouldReturnBoiledEgg() {
        assertEquals(Cheese.class, factory.createTopping().getClass());
    }

    @Test
    public void testCreateFlavorShouldReturnSpicy() {
        assertEquals(Salty.class, factory.createFlavor().getClass());
    }
}

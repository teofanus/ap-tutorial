package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UmamiTest {
    @Test
    public void testUmamiFlavorDescription() {
        Umami umami = new Umami();
        assertEquals("Adding WanPlus Specialty MSG flavoring...", umami.getDescription());
    }
}

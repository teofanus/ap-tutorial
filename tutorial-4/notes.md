# Perbedaan eager instantiation dan lazy instantiation pada Singleton design pattern

## Eager Instantiation

Apa itu eager instantiation? Eager instantiation pada singleton design pattern berarti objek tersebut akan langsung di awal.
Pemanggilan `getInstance()` hanya akan mengembalikan instance yang sudah dibuat tersebut.

### Keuntungan
1. Handling multi-threading lebih mudah karena object akan di-instantiate sejak awal
2. Cocok digunakan apabila sering dilakukan pemanggilan method `getInstance()` karena instance selalu sudah siap

### Kelemahan
1. Introduce Overhead
2. Boros resources apabila tidak sering digunakan

## Lazy Instantiation

Berbeda dengan eager instantiation, lazy instaniation hanya akan membuat objek ketika dibutuhkan saja, contohnya pada
saat pemanggilan method `getInstance()`. Apabila terjadi pemanggilan `getInstance()` ketika instance sudah dibuat maka
`getInstance()` hanya mengembalikan instance yang telah dibuat tersebut.

### Keuntungan
1. Mengurangi overhead
2. Apabila cost untuk membuat instance mahal dan pemanggilan objek tidak sering, maka lazy instantiation akan bermanfaat karena object baru akan
di-instantiate ketika dibutuhkan saja

### Kelemahan
1. Handling multi-threading yang lebih sulit karena object dapat di-instantiate kapan saja.
2. Perlu ada pengecekan berkali-kali apakah instance sudah dibuat atau belum

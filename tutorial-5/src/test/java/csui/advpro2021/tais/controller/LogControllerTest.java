package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = LogController.class)
public class LogControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogServiceImpl logService;
    
    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    private Log log;

    private Mahasiswa mahasiswa;

    private MataKuliah mataKuliah;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718");

        Calendar calendar = Calendar.getInstance();
        calendar.set(2000, 1, 1, 0, 0, 0);
        Date start = calendar.getTime();
        calendar.set(2000, 1, 1, 4, 0, 0);
        Date end = calendar.getTime();

        log = new Log(start, end, "Sebuah Deskripsi");
        log.setIdLog(1);
        log.setMahasiswa(mahasiswa);

        mataKuliah = new MataKuliah("TBA", "Teori Bahasa dan Automata", "Ilkom");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerPostDaftarMahasiswaNotFound() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(null);

        mvc.perform(post("/asdos/" + mahasiswa.getNpm() + "/" + mataKuliah.getKodeMatkul())
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    public void testControllerPostDaftarMataKuliahNotFound() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);

        mvc.perform(post("/asdos/" + mahasiswa.getNpm() + "/" + mataKuliah.getKodeMatkul())
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    public void testControllerUpdateLog() throws Exception {
        when(logService.getLogByIdLog(log.getIdLog())).thenReturn(log);

        Calendar calendar = Calendar.getInstance();
        calendar.set(2000, 1, 1, 0, 0, 0);
        Date start = calendar.getTime();
        calendar.set(2000, 1, 1, 4, 0, 0);
        Date end = calendar.getTime();

        Log log2 = new Log(start, end, "AAAA");

        String logStr = "{\"startTime\":949334400325,\"endTime\":949348800325,\"deskripsi\":\"AAAA\"}";

        when(logService.updateLog(anyInt(), any(Log.class))).thenReturn(log2);
        mvc.perform(put("/log/" + log.getIdLog()).contentType(MediaType.APPLICATION_JSON_VALUE).content(logStr))
                .andExpect(status().isOk());
    }

    @Test
    public void testControllerDeleteLog() throws Exception {
        when(logService.getLogByIdLog(log.getIdLog())).thenReturn(log);

        mvc.perform(delete("/log/" + log.getIdLog())).andExpect(status().isNoContent());
    }

    @Test
    public void testControllerDeleteLogNotFound() throws Exception {
        mvc.perform(delete("/log/" + log.getIdLog())).andExpect(status().isNotFound());
    }

    @Test
    public void testControllerGetSummary() throws Exception {
        mahasiswa.setMatkulAsdos(mataKuliah);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        List<LogSummary> logSummaries = new ArrayList<>();
        logSummaries.add(new LogSummary("1", 1, 1));
        when(logService.getLogSummary(mahasiswa)).thenReturn(logSummaries);

        mvc.perform(get("/log/" + mahasiswa.getNpm() + "/summary")).andExpect(status().isOk())
                .andExpect(jsonPath("$[0].month").value("1"));
    }

    @Test
    public void testControllerGetSummaryMahasiswaNotFound() throws Exception {
        mvc.perform(get("/log/" + mahasiswa.getNpm())).andExpect(status().isNotFound());
    }

    @Test
    public void testControllerGetSummaryMahasiswaNotAsdos() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);

        mvc.perform(get("/log/" + mahasiswa.getNpm())).andExpect(status().isNotImplemented());
    }

    @Test
    public void testControllerGetSummaryMonth() throws Exception {
        mahasiswa.setMatkulAsdos(mataKuliah);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        LogSummary logSummary = new LogSummary("1", 1, 1);
        when(logService.getLogSummaryMonth(mahasiswa, 1)).thenReturn(logSummary);

        mvc.perform(get("/log/" + mahasiswa.getNpm() + "/summary/1")).andExpect(status().isOk());
    }

    @Test
    public void testControllerGetSummaryMonthMahasiswaNotFound() throws Exception {
        mvc.perform(get("/log/" + mahasiswa.getNpm() + "/summary/1")).andExpect(status().isNotFound());
    }

    @Test
    public void testControllerGetSummaryMonthMahasiswaNotAsdos() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);

        mvc.perform(get("/log/" + mahasiswa.getNpm() + "/summary/1")).andExpect(status().isNotImplemented());
    }

    @Test
    public void testControllerPostLog() throws Exception {
        mahasiswa.setMatkulAsdos(mataKuliah);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(logService.createLog(any(Mahasiswa.class), any(Log.class))).thenReturn(log);

        String logstr = "{\"startTime\":949334400325,\"endTime\":949348800325,\"deskripsi\":\"Deskripsi\"}";

        mvc.perform(post("/log/" + mahasiswa.getNpm()).contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(logstr)).andExpect(status().isOk());
    }

    @Test
    public void testControllerPostLogMahasiswaNotFound() throws Exception {
        String logstr = "{\"startTime\":949334400325,\"endTime\":949348800325,\"deskripsi\":\"Deskripsi\"}";

        mvc.perform(post("/log/" + mahasiswa.getNpm()).contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(logstr)).andExpect(status().isNotFound());
    }

    @Test
    public void testControllerPostLogNotAsdos() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);

        String logstr = "{\"startTime\":949334400325,\"endTime\":949348800325,\"deskripsi\":\"Deskripsi\"}";

        mvc.perform(post("/log/" + mahasiswa.getNpm()).contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(logstr)).andExpect(status().isNotImplemented());
    }

    @Test
    public void testControllerGetAllLogs() throws Exception {
        mvc.perform(get("/log?")).andExpect(status().isOk());
    }

    @Test
    public void testControllerGetLogs() throws Exception {
        mahasiswa.setMatkulAsdos(mataKuliah);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        List<Log> resultLogs = new ArrayList<>();
        resultLogs.add(log);
        when(logService.getAllLogsByMahasiswa(mahasiswa)).thenReturn(resultLogs);

        mvc.perform(get("/log/" + mahasiswa.getNpm())).andExpect(status().isOk());
    }

    @Test
    public void testControllerGetLogsMahasiswaNotFound() throws Exception {
        mvc.perform(get("/log/" + mahasiswa.getNpm())).andExpect(status().isNotFound());
    }

    @Test
    public void testControllerGetLogsMahasiswaNotAsdos() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);

        mvc.perform(get("/log/" + mahasiswa.getNpm())).andExpect(status().isNotImplemented());
    }

}

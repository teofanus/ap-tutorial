package csui.advpro2021.tais.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;

@Repository
public interface LogRepository extends JpaRepository<Log, Integer>{
    Log findByIdLog(Integer idLog);
    List<Log> findAllLogByMahasiswa(Mahasiswa mahasiswa);
}

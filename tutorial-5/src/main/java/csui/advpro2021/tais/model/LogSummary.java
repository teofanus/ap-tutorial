package csui.advpro2021.tais.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LogSummary {
    private String month;
    private int jamKerja;
    private long pembayaran;

    public LogSummary(String month, int jamKerja, long pembayaran) {
        this.month = month;
        this.jamKerja = jamKerja;
        this.pembayaran = pembayaran;
    }

    public LogSummary(String month) {
        this(month, 0, 0);
    }

}

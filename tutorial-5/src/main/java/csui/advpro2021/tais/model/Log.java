package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "log")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Log {
    @Id
    @Column(name = "id_log", updatable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer idLog;

    @Column(name = "start_time", nullable = false)
    private Date startTime;

    @Column(name = "end_time", nullable = false)
    private Date endTime;

    @Column(name = "deskripsi")
    private String deskripsi;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "mahasiswa", nullable = false)
    private Mahasiswa mahasiswa;
    
    public Log(Date startTime, Date endTime, String deskripsi) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.deskripsi = deskripsi;
    }
}

package csui.advpro2021.tais.service;

import java.util.Collection;
import java.util.List;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;

public interface LogService {
    Log createLog(Mahasiswa mahsiswa, Log log);

    Log getLogByIdLog(Integer idLog);

    List<Log> getAllLogsByMahasiswa(Mahasiswa mahasiswa);

    List<Log> getAllLogs();

    Log updateLog(Integer idLog, Log log);

    void deleteLogByIdLog(Integer idLog);

    Collection<LogSummary> getLogSummary(Mahasiswa mahasiswa);

    LogSummary getLogSummaryMonth(Mahasiswa mahasiswa, int month);
}
